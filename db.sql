CREATE TABLE `log` (
 `date` date NOT NULL,
 `generated` decimal(10,2) NOT NULL,
 `consumed` decimal(10,2) NOT NULL,
 PRIMARY KEY (`date`)
);
