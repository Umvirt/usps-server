<?php

include "sun.php";
//https://github.com/davemorrissey/sunrise-sunset-map
date_default_timezone_set("Asia/Yakutsk");

$utc = new DateTimeZone('UTC');
$dateUtc = new DateTime(date("Y-m-d"), $utc);

$nowdate=date_create();

$ss=new SunService();
$res=$ss->calcDay($dateUtc,52,-113,timezone_open("Asia/Yakutsk"));
//$res=$ss->calcDay(date_create("2018-06-19 04:00:00"),52,113,timezone_open("Asia/Yakutsk"));
//$res2=$ss->calcPosition(date_create(),52,113);

$daylen=date_diff($res->sunset,$res->sunrise)->format("%h:%i");
$sunrest=date_diff($res->sunset,$nowdate)->format("%h:%i");

//var_dump($res, $daylen,$sunrest, $nowdate);
$cdate=$nowdate->format("Y-m-d");
$ctime=$nowdate->format("H:i");

$sunrise=$res->sunrise->format("H:i");
$sunset=$res->sunset->format("H:i");
$noon=$res->transit->format("H:i");


$res=array();

$res['current_date']=$cdate;
$res['current_time']=$ctime;
$res['sunrise']=$sunrise;
$res['noon']=$noon;
$res['sunset']=$sunset;


$redis = new Redis();
$redis->connect('127.0.0.1');
$data=$redis->get('spsdata');
$time=$redis->get('spstime');
$redis->close();

$lastreport=intval($time);
$sps=json_decode($data);

if(time()-$lastreport<=65){
$res['status']="up";
$res['sps']=$sps;

if($sps->battery_current>=0){
$res['src']="PV";
}else{
$res['src']="Battery";
if($sps->battery_charging_current){
$res['src'].="+PV";
}
}

}else{
$res['status']="down";
}

header("Content-type: text/json");
echo json_encode($res);
?>
