<html>
<head>
<title>Solar Power Station</title>
<meta http-equiv="refresh" content="60">
</head>
<body>
<?php

include "sun.php";
//https://github.com/davemorrissey/sunrise-sunset-map
date_default_timezone_set("Asia/Yakutsk");

$utc = new DateTimeZone('UTC');
$dateUtc = new DateTime(date("Y-m-d"), $utc);

$nowdate=date_create();

$ss=new SunService();
$res=$ss->calcDay($dateUtc,52,-113,timezone_open("Asia/Yakutsk"));
//$res=$ss->calcDay(date_create("2018-06-19 04:00:00"),52,113,timezone_open("Asia/Yakutsk"));
//$res2=$ss->calcPosition(date_create(),52,113);

$daylen=date_diff($res->sunset,$res->sunrise)->format("%h:%i");
$sunrest=date_diff($res->sunset,$nowdate)->format("%h:%i");

//var_dump($res, $daylen,$sunrest, $nowdate);
$cdate=$nowdate->format("Y-m-d");
$ctime=$nowdate->format("H:i");

$sunrise=$res->sunrise->format("H:i");
$sunset=$res->sunset->format("H:i");
$noon=$res->transit->format("H:i");



echo "<h1>Solar Power Station</h1><hr>
Current Date: $cdate<br>
Current Time: $ctime<br>
Sunrise: $sunrise<br>
Noon: $noon<br>
Sunset: $sunset ($sunrest)<br>
";
echo "<hr>";

$redis = new Redis();
$redis->connect('127.0.0.1');
$data=$redis->get('spsdata');
$time=$redis->get('spstime');
//$redis->close();

$lastreport=intval($time);
$sps=json_decode($data);

var_dump($sps,$redis->get('spslogdebug'));

include "config.php";
include "db.php";
$db=new db_connection($db_config);
$sps=json_decode($data);
$sql="insert into log (date, generated, consumed) values
(\"".addslashes($sps->current_date)."\",
\"".addslashes($sps->generated_energy_today)."\",
\"".addslashes($sps->consumed_energy_today)."\")";
$redis->set('spslogdebug',$sql);
$db->execute($sql);


$redis->close();

