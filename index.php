<html>
<head>
<title>Solar Power Station</title>
<meta http-equiv="refresh" content="60">
</head>
<body>
<?php

include "sun.php";
//https://github.com/davemorrissey/sunrise-sunset-map
date_default_timezone_set("Asia/Yakutsk");

$utc = new DateTimeZone('UTC');
$dateUtc = new DateTime(date("Y-m-d"), $utc);

$nowdate=date_create();

$ss=new SunService();
$res=$ss->calcDay($dateUtc,52,-113,timezone_open("Asia/Yakutsk"));
//$res=$ss->calcDay(date_create("2018-06-19 04:00:00"),52,113,timezone_open("Asia/Yakutsk"));
//$res2=$ss->calcPosition(date_create(),52,113);

$daylen=date_diff($res->sunset,$res->sunrise)->format("%h:%i");
$sunrest=date_diff($res->sunset,$nowdate)->format("%h:%i");

//var_dump($res, $daylen,$sunrest, $nowdate);
$cdate=$nowdate->format("Y-m-d");
$ctime=$nowdate->format("H:i");

$sunrise=$res->sunrise->format("H:i");
$sunset=$res->sunset->format("H:i");
$noon=$res->transit->format("H:i");



echo "<h1>Solar Power Station</h1><hr>
Current Date: $cdate<br>
Current Time: $ctime<br>
Sunrise: $sunrise<br>
Noon: $noon<br>
Sunset: $sunset ($sunrest)<br>
";
echo "<hr>";

$redis = new Redis();
$redis->connect('127.0.0.1');
$data=$redis->get('spsdata');
$time=$redis->get('spstime');
$redis->close();

$lastreport=intval($time);
$sps=json_decode($data);

if(time()-$lastreport<=65){
echo "Status: UP";

if($sps->battery_current>=0){
$src="PV";
}else{
$src="Battery";
if($sps->battery_charging_current){
$src.="+PV";
}
}

echo "<br>Source: $src";
echo "<br>Controller date: $sps->current_date";
echo "<br>Controller time: $sps->current_time";

echo "<hr>";
echo "PV voltage(V): $sps->pv_voltage<br>";
echo "PV current(A): $sps->pv_current<br>";
echo "PV power(W): $sps->pv_power<br>";

echo "Battery voltage(V): $sps->battery_voltage<br>";
echo "Battery current(A): $sps->battery_current<br>";
echo "Battery charging current(A): $sps->battery_charging_current<br>";
echo "Battery charging power(W): $sps->battery_charging_power<br>";

echo "Load voltage(V): $sps->load_voltage<br>";
echo "Load current(A): $sps->load_current<br>";
echo "Load power(W): $sps->load_power<br>";

echo "<hr>";

echo "Generated energy (KWh) today: $sps->generated_energy_today<br>";
echo "Generated energy (KWh) month: $sps->generated_energy_month<br>";
echo "Generated energy (KWh) year: $sps->generated_energy_year<br>";
echo "Generated energy (KWh) total: $sps->generated_energy_total<br>";

echo "<hr>";

echo "Consumed energy (KWh) today: $sps->consumed_energy_today<br>";
echo "Consumed energy (KWh) month: $sps->consumed_energy_month<br>";
echo "Consumed energy (KWh) year: $sps->consumed_energy_year<br>";
echo "Consumed energy (KWh) total: $sps->consumed_energy_total<br>";

echo "<hr>";

echo "Ambient temperature (C): $sps->ambient_temperature<br>";
echo "Charger temperature (C): $sps->charger_temperature<br>";
echo "Heatsink temperature (C): $sps->heatsink_temperature<br>";
echo "Battery temperature (C): $sps->battery_temperature<br>";


}else{
echo "Status: DOWN";
}
?>
</body>
</html>
